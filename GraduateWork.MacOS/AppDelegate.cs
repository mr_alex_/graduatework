﻿using AppKit;
using Foundation;
using GraduateWork;
using Prism;
using Prism.Ioc;
using Xamarin.Forms;
using Xamarin.Forms.Platform.MacOS;

[Register("AppDelegate")]
public class AppDelegate : FormsApplicationDelegate
{
    NSWindow window;
    public AppDelegate()
    {
        var style = NSWindowStyle.Closable | NSWindowStyle.Titled;

        var rect = new CoreGraphics.CGRect(200, 1000, 1280, 720);

        window = new NSWindow(rect, style, NSBackingStore.Buffered, false);
        window.Title = "Graduate Work";
        window.TitleVisibility = NSWindowTitleVisibility.Visible;
    }

    public override NSWindow MainWindow
    {
        get { return window; }
    }

    public override void DidFinishLaunching(NSNotification notification)
    {
        Forms.Init();
        LoadApplication(new App());
        base.DidFinishLaunching(notification);
    }
}

public class iOSInitializer : IPlatformInitializer
{
    public void RegisterTypes(IContainerRegistry containerRegistry)
    {
    }
}