﻿using System.Collections.Generic;
using Prism.Mvvm;

namespace GraduateWork.Models
{
    // Результуюча модель, що містить дані аналізу відповідних View та ViewModel файлів.
    // Використовується сутностями IFileServiceEditor, MvvmAnalyzer для збереження результатів аналізу,
    // та AnalyzedMvvmFileHelper і PatternsAnalyzeResultViewModel для відображення в UI
    public class AnalyzedMvvmFileModel : BindableBase
    {
        public string ViewName { get; set; }
        public bool ViewHasReferences { get; set; }
        public bool ViewHasBindedProperties { get; set; }

        public string ViewModelName { get; set; }
        public bool ViewModelHasReferences { get; set; }
        public bool ViewModelIsBindable { get; set; }

        public AnalyzedMvvmFileModel()
        {
            ViewName = "Not Found";
            ViewModelName = "Not Found";
        }
    }
}
