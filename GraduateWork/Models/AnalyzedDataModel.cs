﻿using System;
namespace GraduateWork.Models
{
    // Базова модель, що містить базові властивості необхідні для аналізу певного класу
    public abstract class AnalyzedDataModel
    {
        public string Name;

        public bool HasReferences;
        public bool HasCorrectSuffix;
    }
}
