﻿using System;
using System.Collections.Generic;

namespace GraduateWork.Models
{
    // Модель, що використовується для аналізу реалізації користувацького інтерфейсу (UI),
    // який представлений на рівні View у вигляді файлів з розширенням .xaml.
    // Використовується сутністю MvvmAnalyzer для збереження результатів аналізу файлу руалізації UI.
    public class AnylyzedViewDataModel : AnalyzedDataModel
    {
        public bool HasXamlLayout;
        public bool HasAdditionalCodeBehindLogic;

        public List<string> BindingProperties;
    }
}
