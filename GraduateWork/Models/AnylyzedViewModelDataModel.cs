﻿using System;
namespace GraduateWork.Models
{
    // Модель, що використовується для аналізу реалізації логіки користувацького інтерфейсу (UI),
    // що представлена на рівні ViewModel у вигляді файлів з розширенням .cs
    // Використовується сутністю MvvmAnalyzer для збереження результатів аналізу файлу руалізації логіки UI.
    public class AnylyzedViewModelDataModel : AnalyzedDataModel
    {
        public bool ImplementsBindableInterface;
    }
}
