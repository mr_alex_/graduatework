﻿using System;

namespace GraduateWork.Enums
{
    // Структура даних для опису розмірів мобільних додатків
    // Використовується AnalyzerSettingsViewModel та AppSizeToStringConverter для відображення в UI
    public enum AppSize
    {
        Small,
        Medium,
        Big
    }
}
