﻿using System;
namespace GraduateWork.Enums
{
    // Структура данних для опису типу файлів, що аналізуються.
    // Використовується сутностями AppFileService та FilesListViewModel
    // для завантаження та відображення в Ui списку файлів проекту.
    public enum FileType
    {
        cs,
        xaml
    }
}
