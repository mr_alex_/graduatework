﻿using System;
using System.Globalization;
using GraduateWork.Enums;
using Xamarin.Forms;

namespace GraduateWork.Converters
{
    public class AppSizeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string currentValue = (value as string).ToLower();

            AppSize convertedValue;

            switch (currentValue)
            {
                case "small":
                    convertedValue = AppSize.Small;
                    break;

                case "medium":
                    convertedValue = AppSize.Medium;
                    break;

                case "big":
                    convertedValue = AppSize.Big;
                    break;

                default:
                    convertedValue = AppSize.Small;
                    break;
            }

            return convertedValue;
        }
    }
}
