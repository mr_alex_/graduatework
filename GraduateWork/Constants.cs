﻿using System;
using System.Collections.Generic;
using GraduateWork.Enums;

namespace GraduateWork
{
    public static class AppConstants
    {
        public static readonly List<string> AppTypes = new List<string>
        {
            "Xamarin / .Net",
            "Flutter / Dart",
            "React Native / JavaScript"
        };

        public static Dictionary<AppSize, string> TimeToDevelop =
            new Dictionary<AppSize, string>()
            {
                { AppSize.Small, "to 6 months" },
                { AppSize.Medium, "6 months - year" },
                { AppSize.Big, "more than year" },
            };

        public static Dictionary<AppSize, int> AppBudget =
            new Dictionary<AppSize, int>()
            {
                { AppSize.Small, 60000 },
                { AppSize.Medium, 150000 },
                { AppSize.Big, 300000 },
            };
    }

    public static class NavigationConstants
    {
        public static readonly string AppDirectoryPath = "app_directory_path";
        public static readonly string AppFilesPaths = "app_files_paths";
    }
}
