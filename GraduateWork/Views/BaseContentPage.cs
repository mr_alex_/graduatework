﻿using Xamarin.Forms;

namespace GraduateWork.Views
{
    public class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            NavigationPage.SetHasBackButton(this, false);
        }
    }
}
