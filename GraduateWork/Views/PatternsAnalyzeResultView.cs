﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GraduateWork.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PatternsAnalyzeResultView : BaseContentPage
    {
        public PatternsAnalyzeResultView()
        {
            InitializeComponent();
        }
    }
}
