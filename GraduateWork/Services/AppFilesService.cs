﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using GraduateWork.Enums;
using GraduateWork.Helpers;
using GraduateWork.Models;
using Xamarin.Forms.Internals;

namespace GraduateWork.Services
{
    public class AppFilesService : IAppFilesService
    {
        public string DirectoryPath { get; private set; }

        List<string> _filesPaths;

        public IEnumerable<string> GetAllFilesPaths(string directoryPath, IEnumerable<FileType> fileTypes = null)
        {
            DirectoryPath = directoryPath;

            List<string> files = new List<string>();

            if (File.Exists(directoryPath))
            {
                // This path is a file
                files.Add(directoryPath);

                return files;
            }

            if (Directory.Exists(directoryPath))
            {
                // This path is a directory
                files.AddRange(ProcessFilesInDirectory(directoryPath, fileTypes));
            }
            else
            {
                Console.WriteLine("{0} is not a valid file or directory.", directoryPath);
            }

            _filesPaths = new List<string>(files);

            return files;
        }

        public string GetFullFilePathFromName(string fileName)
        {
            foreach (var path in _filesPaths)
            {
                if (path.Substring(path.LastIndexOf("/") + 1).Contains(fileName))
                {
                    return path;
                }
            }

            return null;
        }

        public int GetFileLinesCount(string filePath)
        {
            var fileContent = ReadFileContent(filePath);

            return fileContent != null ? fileContent.Count() : 0;
        }

        public int GetTotalLinesCount(List<string> filesPaths)
        {
            int linesCount = 0;

            foreach (var filePath in filesPaths)
            {
                var fileContent = ReadFileContent(filePath);
                linesCount += fileContent.Count();
            }

            return linesCount;
        }

        public string GetShortFileNameFromPath(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf("/") + 1);
        }

        public string LoadFileContent(string filePath)
        {
            string text = File.ReadAllText(filePath);

            return text;
        }

        public IEnumerable<string> ReadFileContent(string filePath)
        {
            var text = File.ReadLines(filePath);

            return text;
        }

        // Process all files in the directory passed in, recurse on any directories
        // that are found, and process the files they contain.
        private IEnumerable<string> ProcessFilesInDirectory(string targetDirectory, IEnumerable<FileType> fileTypes)
        {
            List<string> filesListByType = new List<string>();

            // Process the list of files found in the directory.
            var loadedFiles = Directory.GetFiles(targetDirectory);

            if (fileTypes != null && fileTypes.ToList().Count > 0)
            {
                fileTypes.ForEach((type) => {
                    var fileTypeAsString = type.ToString().ToLower();

                    var files = loadedFiles.Where(
                        fileName => fileName.EndsWith("." + fileTypeAsString)
                    ).ToList();

                    filesListByType.AddRange(files);
                });
            }
            else
            {
                filesListByType.AddRange(loadedFiles);
            }

            // Recurse into subdirectories of this directory.
            var subdirectoryEntries = Directory.GetDirectories(targetDirectory);

            foreach (string subdirectory in subdirectoryEntries)
            {
                bool needProccesSubdirectory = !subdirectory.EndsWith("obj")
                    && !subdirectory.EndsWith("bin");

                if (needProccesSubdirectory)
                {
                    filesListByType.AddRange(ProcessFilesInDirectory(subdirectory, fileTypes));
                }
            }

            return filesListByType;
        }

        public void SaveToFile(string path, string text)
        {
            //Open the stream and write to it.
            using (FileStream fs = File.OpenWrite(path))
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(text);

                // Add some information to the file.
                fs.Write(info, 0, info.Length);
            }
        }

        public void SaveAnalyzedMvvmFilesContent(
            string path,
            List<AnalyzedMvvmFileModel> analyzedMvvmResults)
        {
            string fileContent = string.Empty;

            foreach (var mvvm in analyzedMvvmResults)
            {
                if (fileContent == string.Empty)
                {
                    string currentDateAsString = DateTime.Now.ToLocalTime().ToLongDateString();

                    string header = "Analysed MVVM files content.";

                    fileContent += currentDateAsString + "\n";
                    fileContent += header + "\n";
                    fileContent += separatorLineAsString(header.Length) + "\n\n";

                    string totalLinesCount = "Total lines count of the project: "
                        + GetTotalLinesCount(new List<string>(GetAllFilesPaths(DirectoryPath))).ToString();

                    fileContent += totalLinesCount + "\n";
                    fileContent += separatorLineAsString(totalLinesCount.Length) + "\n\n";
                }

                var viewFilePath = GetFullFilePathFromName(mvvm.ViewName);

                if (viewFilePath != null)
                {
                    string header = mvvm.ViewName;

                    fileContent += header + "\n\n";
                    fileContent += LoadFileContent(viewFilePath) + "\n";
                    fileContent += separatorLineAsString(80) + "\n\n";
                }

                var viewModelFilePath = GetFullFilePathFromName(mvvm.ViewModelName);

                if (viewModelFilePath != null)
                {
                    string header = mvvm.ViewModelName;

                    fileContent += header + "\n\n";
                    fileContent += LoadFileContent(viewModelFilePath) + "\n";
                    fileContent += separatorLineAsString(80) + "\n\n";
                }
            }

            if (fileContent != string.Empty)
            {
                SaveToFile(path, fileContent);
            }
        }

        public void SaveAnalyzedMvvmInfo(
            string path,
            List<AnalyzedMvvmFileModel> analyzedMvvmResults)
        {
            string currentDateAsString = DateTime.Now.ToLocalTime().ToLongDateString();

            const string hasReferences = "Has references";
            const string hasNoReferences = "Has No references";

            const string hasBindedProperties = "Has binded properties";
            const string hasNoBindedProperties = "Has No binded properties";

            const string IsBindable = "Is bindable";
            const string IsNotBindable = "Is Not bindable";

            string mvvmText = null;

            int linesCount = 1;

            foreach (var result in analyzedMvvmResults)
            {
                if (mvvmText == null)
                {
                    string header = "MVVM pattern analysing info.";

                    mvvmText += currentDateAsString + "\n";
                    mvvmText += header + "\n";
                    mvvmText += separatorLineAsString(header.Length) + "\n\n";

                    int viewsWithViewModelsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewHasBindedProperties, analyzedMvvmResults);

                    string totalViewsCount = "Total Views count: "
                        + AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewName.Contains(".xaml"), analyzedMvvmResults);

                    string viewsWithRefsCount = "Views with refs count: "
                        + AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewHasReferences, analyzedMvvmResults);

                    string viewsMatchedViewModelsCount = "Views matched with ViewModels count: " + viewsWithViewModelsCount;

                    string totalViewModelsCount = "Total View models count: "
                        + AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelName.Contains(".cs"), analyzedMvvmResults);

                    string viewModelsMatchedViewsCount = "ViewModels matched with Views count: " + viewsWithViewModelsCount;

                    string viewModelsWithRefsCount = "ViewModels with refs count: " +
                        AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelHasReferences, analyzedMvvmResults);

                    string bindableViewModelsCount = "Bindable ViewModels count: " +
                        AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelIsBindable, analyzedMvvmResults);

                    string mvvmShortInfo = totalViewsCount + "\n"
                        + viewsWithRefsCount + "\n"
                        + viewsMatchedViewModelsCount + "\n"
                        + totalViewModelsCount + "\n"
                        + viewModelsMatchedViewsCount + "\n"
                        + viewModelsWithRefsCount + "\n"
                        + bindableViewModelsCount + "\n";

                    mvvmText += mvvmShortInfo;
                    mvvmText += separatorLineAsString(bindableViewModelsCount.Length) + "\n\n";
                }

                string mvvmInfoLine = linesCount + ". "
                    + result.ViewName + ": "
                    + (result.ViewHasReferences ? hasReferences : hasNoReferences) + ", "
                    + (result.ViewHasBindedProperties ? hasBindedProperties : hasNoBindedProperties) + ", "
                    + result.ViewModelName + ": "
                    + (result.ViewModelHasReferences ? hasReferences : hasNoReferences) + ", "
                    + (result.ViewModelIsBindable ? IsBindable : IsNotBindable) + "\n\n";

                mvvmText += mvvmInfoLine;
                mvvmText += separatorLineAsString(mvvmInfoLine.Length) + "\n\n";

                linesCount++;
            }

            if (mvvmText != null && mvvmText != string.Empty)
            {
                SaveToFile(path, mvvmText);
            }
        }

        private string separatorLineAsString(int length)
        {
            string separator = string.Empty;

            for (int i = 0; i < length; i++)
            {
                separator += "_";
            }

            return separator;
        }
    }
}
