﻿using System;
using System.Collections.Generic;
using GraduateWork.Models;

namespace GraduateWork.Services
{
    public interface IMvvmAnalyzer
    {
        List<AnalyzedMvvmFileModel> AnalyzeMvvmImplementation(IEnumerable<string> filePaths);

        AnylyzedViewDataModel AnalyzeViewXamlFile(string filePath);
        AnylyzedViewModelDataModel AnalyzeViewModelFile(string filePath);

        List<AnylyzedViewDataModel> AnalyzeViews(IEnumerable<string> filePaths);
        List<AnylyzedViewModelDataModel> AnalyzeViewModels(IEnumerable<string> filePaths);
    }
}
