﻿using System;
using System.Collections.Generic;
using GraduateWork.Models;

namespace GraduateWork.Services
{
    public interface IFileEditorService
    {
        List<string> RemoveEmptyLines(List<string> fileContent);

        List<string> RemoveCommentedLines(
            List<string> fileContent,
            string startCommentSymbol,
            string endCommentSymbol);
    }
}
