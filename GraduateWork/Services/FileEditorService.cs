﻿using System;
using System.Collections.Generic;
using GraduateWork.Helpers;
using GraduateWork.Models;
using GraduateWork.Services;

namespace GraduateWork.Services
{
    public class FileEditorService : IFileEditorService
    {
        private IAppFilesService _appFilesService;

        public FileEditorService(IAppFilesService appFilesService)
        {
            _appFilesService = appFilesService;
        }

        public List<string> RemoveEmptyLines(List<string> fileContent)
        {
            List<string> fileWithoutEmptyLines = new List<string>();

            fileContent.ForEach(line =>
            {
                if (line != string.Empty)
                {
                    fileWithoutEmptyLines.Add(line);
                }
            });

            return fileWithoutEmptyLines;
        }

        public List<string> RemoveCommentedLines(
            List<string> fileContent,
            string startCommentSymbol,
            string endCommentSymbol)
        {
            List<string> fileWithoutComments = new List<string>();

            bool isCommentedLine = false;

            foreach (var line in fileContent)
            {
                if (line.StartsWith(startCommentSymbol))
                {
                    isCommentedLine = true;
                }
                else if (line.EndsWith(endCommentSymbol))
                {
                    isCommentedLine = false;
                    continue;
                }

                if (isCommentedLine)
                {
                    continue;
                }
                else
                {
                    fileWithoutComments.Add(line);
                }
            }

            return fileWithoutComments;
        }

        private List<string> RemoveSingleCommentedLines(
            List<string> fileContent,
            string commentSymbol)
        {
            List<string> fileWithoutComments = new List<string>();

            foreach (var line in fileContent)
            {
                if (line.StartsWith(commentSymbol))
                {
                    continue;
                }
                else
                {
                    fileWithoutComments.Add(line);
                }
            }

            return fileWithoutComments;
        }
    }
}
