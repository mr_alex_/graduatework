﻿using System;
using System.Collections.Generic;
using GraduateWork.Enums;
using GraduateWork.Models;

namespace GraduateWork.Services
{
    public interface IAppFilesService
    {
        string DirectoryPath { get; }

        IEnumerable<string> GetAllFilesPaths(string directiryPath, IEnumerable<FileType> fileTypes = null);

        string LoadFileContent(string filePath);

        IEnumerable<string> ReadFileContent(string filePath);

        string GetShortFileNameFromPath(string filePath);

        string GetFullFilePathFromName(string fileName);

        int GetFileLinesCount(string filePath);

        int GetTotalLinesCount(List<string> filesPaths);

        void SaveToFile(string path, string text);

        void SaveAnalyzedMvvmInfo(string path, List<AnalyzedMvvmFileModel> analyzedMvvmResults);
        void SaveAnalyzedMvvmFilesContent(string path, List<AnalyzedMvvmFileModel> analyzedMvvmResults);
    }
}