﻿using System;
using System.Collections.Generic;
using System.Linq;
using GraduateWork.Models;

namespace GraduateWork.Services
{
    public class MvvmAnalyzer : IMvvmAnalyzer
    {
        const string ViewXamlKeyWord = "View.xaml";
        const string ViewModelCsKeyWord = "ViewModel.cs";

        private IAppFilesService _filesService;
        private IFileEditorService _fileEditorService;

        private List<string> _filePaths;

        private Dictionary<string, List<string>> _viewModelFilesContent;

        public MvvmAnalyzer(
            IAppFilesService filesService,
            IFileEditorService fileEditorService)
        {
            _filesService = filesService;
            _fileEditorService = fileEditorService;

            _viewModelFilesContent = new Dictionary<string, List<string>>();
        }

        public List<AnalyzedMvvmFileModel> AnalyzeMvvmImplementation(IEnumerable<string> filePaths)
        {
            _filePaths = filePaths.ToList();

            var anylizedViews = AnalyzeViews(filePaths);

            var anylizedViewModels = AnalyzeViewModels(filePaths);

            var analyzedMvvmFiles = new List<AnalyzedMvvmFileModel>();

            if (anylizedViews != null && anylizedViews.Count > 0)
            {
                foreach (var view in anylizedViews)
                {
                    var analyzedMvvmFile = new AnalyzedMvvmFileModel();

                    analyzedMvvmFile.ViewName = view.Name;
                    analyzedMvvmFile.ViewHasReferences = view.HasReferences;

                    if (anylizedViewModels != null && anylizedViewModels.Count > 0)
                    {
                        var viewModel = GetViewModelByViewName(view.Name, anylizedViewModels);

                        if (viewModel != null)
                        {
                            analyzedMvvmFile.ViewModelName = viewModel.Name;
                            analyzedMvvmFile.ViewModelHasReferences = viewModel.HasReferences;

                            analyzedMvvmFile.ViewHasBindedProperties = CheckViewBindedWithViewModel(
                                GetViewByName(view.Name, anylizedViews), viewModel);

                            analyzedMvvmFile.ViewModelIsBindable = viewModel.ImplementsBindableInterface;
                        }
                    }


                    analyzedMvvmFiles.Add(analyzedMvvmFile);
                }
            }

            if (anylizedViewModels != null && anylizedViewModels.Count > 0)
            {
                foreach (var analyzedViewModel in anylizedViewModels)
                {
                    bool viewModelAdded = analyzedMvvmFiles.FirstOrDefault(
                        item => item.ViewModelName == analyzedViewModel.Name) == null ? false : true;

                    if (!viewModelAdded)
                    {
                        var analyzedMvvmFile = new AnalyzedMvvmFileModel();

                        analyzedMvvmFile.ViewModelName = analyzedViewModel.Name;
                        analyzedMvvmFile.ViewModelHasReferences = analyzedViewModel.HasReferences;
                        analyzedMvvmFile.ViewModelIsBindable = analyzedViewModel.ImplementsBindableInterface;

                        analyzedMvvmFiles.Add(analyzedMvvmFile);
                    }
                }
            }

            return analyzedMvvmFiles;
        }

        public List<AnylyzedViewDataModel> AnalyzeViews(IEnumerable<string> filePaths)
        {
            List<AnylyzedViewDataModel> anylizedViews = new List<AnylyzedViewDataModel>();

            foreach (var path in filePaths)
            {
                if (path.EndsWith(ViewXamlKeyWord))
                {
                    var anylyzedView = AnalyzeViewXamlFile(path);
                    anylizedViews.Add(anylyzedView);
                }
            }

            return anylizedViews;
        }

        public List<AnylyzedViewModelDataModel> AnalyzeViewModels(IEnumerable<string> filePaths)
        {
            List<AnylyzedViewModelDataModel> anylizedViewModels = new List<AnylyzedViewModelDataModel>();

            foreach (var path in filePaths)
            {
                if (path.Contains(ViewModelCsKeyWord))
                {
                    var anylyzedViewModel = AnalyzeViewModelFile(path);
                    anylizedViewModels.Add(anylyzedViewModel);
                }
            }

            return anylizedViewModels;
        }

        public AnylyzedViewModelDataModel AnalyzeViewModelFile(string filePath)
        {
            AnylyzedViewModelDataModel anylyzedViewModel = new AnylyzedViewModelDataModel();

            anylyzedViewModel.HasCorrectSuffix = filePath.EndsWith(ViewModelCsKeyWord);

            if (anylyzedViewModel.HasCorrectSuffix)
            {
                var content = _fileEditorService.RemoveEmptyLines(
                    _filesService.ReadFileContent(filePath).ToList()
                );

                anylyzedViewModel.Name = _filesService.GetShortFileNameFromPath(filePath);

                anylyzedViewModel.HasReferences = isFileHaveReferences(anylyzedViewModel.Name);

                anylyzedViewModel.ImplementsBindableInterface = isBindableViewModel(content, anylyzedViewModel.Name);

                _viewModelFilesContent.Add(anylyzedViewModel.Name, content);
            }

            return anylyzedViewModel;
        }

        private bool isBindableViewModel (List<string> viewModelContent, string fileName)
        {
            const string InotifyPropertyChanged = "INotifyPropertyChanged";
            const string BindableBase = "BindableBase";

            string viewModelName = NameWithoutType(fileName);

            bool isBindableViewmodel = false;

            foreach (var line in viewModelContent)
            {
                bool viewModelHasInheritance = line.Contains("class")
                    && line.Contains(viewModelName)
                    && line.Contains(":") && !line.Contains("base");

                if (viewModelHasInheritance)
                {
                    string inheritedInterfaceName = line.Substring(line.LastIndexOf(":") + 2);

                    for (int itemIndex = 0; itemIndex < inheritedInterfaceName.Length; itemIndex++)
                    {
                        if (inheritedInterfaceName[itemIndex] == ',')
                        {
                            inheritedInterfaceName = inheritedInterfaceName.Remove(itemIndex);
                            break;
                        }
                    }

                    bool isBindableInheritedInterface = inheritedInterfaceName == InotifyPropertyChanged
                        || inheritedInterfaceName == BindableBase;

                    if (isBindableInheritedInterface)
                    {
                        isBindableViewmodel = true;
                        break;
                    }
                    else
                    {
                        string fullFilePath = null;

                        foreach(var path in _filePaths)
                        {
                            var filePathWithSpaces = path.Replace('/', ' ');

                            if (filePathWithSpaces.EndsWith(inheritedInterfaceName + ".cs"))
                            {
                                fullFilePath = path;
                                break;
                            }
                        }

                        if (fullFilePath != null)
                        {
                            var content = _fileEditorService.RemoveEmptyLines(
                                _filesService.ReadFileContent(fullFilePath).ToList()
                            );

                            isBindableViewmodel = isBindableViewModel(content, inheritedInterfaceName);
                        }
                    }
                }
            }

            return isBindableViewmodel;
        }

        // ToDo:
        // 1. Check if file content has mistakes
        // (for example it has no some symbols (<, >, />) in opening or closing page tag)
        public AnylyzedViewDataModel AnalyzeViewXamlFile(string filePath)
        {
            AnylyzedViewDataModel anylyzedView = new AnylyzedViewDataModel();

            anylyzedView.HasCorrectSuffix = filePath.EndsWith(ViewXamlKeyWord);

            if (anylyzedView.HasCorrectSuffix)
            {
                List<string> content = PrepareViewFileContent(_filesService.ReadFileContent(filePath).ToList());

                bool isOpeningTagCorrect = isOpeningViewRootTagCorrect(content);
                bool isClosingTagCorrect = isClosingViewRootTagCorrect(content);

                anylyzedView.Name = _filesService.GetShortFileNameFromPath(filePath);

                anylyzedView.HasReferences = isFileHaveReferences(anylyzedView.Name);

                anylyzedView.HasXamlLayout = isOpeningTagCorrect && isClosingTagCorrect;

                anylyzedView.BindingProperties = new List<string>(FindViewBindingProperties(content));
            }

            return anylyzedView;
        }

        private bool isFileHaveReferences(string name)
        {
            string formattedName = NameWithoutType(name);

            bool hasReferences = false;

            if (_filePaths != null)
            {
                foreach (var path in _filePaths)
                {
                    var content = _filesService.LoadFileContent(path);

                    if (path.EndsWith(name) || path.EndsWith(name + ".cs"))
                    {
                        continue;
                    }

                    if (content.Contains(formattedName))
                    {
                        hasReferences = true;
                        break;
                    }
                }
            }

            return hasReferences;
        }

        private List<string> FindViewBindingProperties (List<string> fileContent)
        {
            const string BindingKeyWord = "Binding";

            var bindingProperties = new List<string>();

            fileContent.ForEach(line =>
            {
                if (line.Contains(BindingKeyWord + " "))
                {
                    int from = line.LastIndexOf(BindingKeyWord + " " );
                    string lineWithPropertyName = line.Substring(from);

                    string[] splittedLine = lineWithPropertyName.Split(' ');

                    string propertyName = string.Empty;

                    // A property name index == 1
                    if (splittedLine[1].Contains(","))
                    {
                        propertyName = splittedLine[1].Remove(splittedLine[1].LastIndexOf(","));
                    }
                    else if (splittedLine[1].Contains("}"))
                    {
                        propertyName = splittedLine[1].Remove(splittedLine[1].LastIndexOf("}"));
                    }
                    else
                    {
                        propertyName = splittedLine[1];
                    }

                    Console.WriteLine("Property name: " + propertyName);

                    bindingProperties.Add(propertyName);
                }
            });

            return bindingProperties;
        }

        private bool isOpeningViewRootTagCorrect(List<string> fileContent)
        {
            bool firstOpeningTagSymbolFound = false;
            bool lastOpeningTagSymbolFound = false;

            bool hasXamarinFormsLink = false;
            bool hasXamlLink = false;

            // Usually an opening tag contains several lines
            foreach (var line in fileContent)
            {
                // Skip a line with xml version which usually looks like this:
                // <?xml version="1.0" encoding="UTF-8"?>
                if (line.StartsWith("<?xml version="))
                {
                    continue;
                }

                // All Xamarin.Forms opening page tags contain 'Page' word and <, > symbols
                if (!firstOpeningTagSymbolFound)
                {
                    firstOpeningTagSymbolFound = line.StartsWith("<")
                        && line.Contains("Page>");
                }

                if (firstOpeningTagSymbolFound && lastOpeningTagSymbolFound)
                {
                    break;
                }

                if (!hasXamarinFormsLink)
                {
                    hasXamarinFormsLink = line.Contains("http://xamarin.com/schemas/")
                        && line.Contains("/forms");
                }

                bool lineContainsXamlLink = line.Contains("http://schemas.microsoft.com/winfx/")
                    && line.Contains("/xaml");

                if (!hasXamlLink)
                {
                    hasXamlLink = lineContainsXamlLink;
                }

                lastOpeningTagSymbolFound = line.EndsWith(">");
            }

            return firstOpeningTagSymbolFound
                && lastOpeningTagSymbolFound
                && hasXamarinFormsLink
                && hasXamlLink;
        }

        private bool isClosingViewRootTagCorrect(List<string> fileContent)
        {
            // All Xamarin.Forms closing page tags contain 'Page' word and </, > symbols
            return fileContent.Last().StartsWith("</")
                && fileContent.Last().Contains("Page>");
        }

        private bool CheckViewBindedWithViewModel(AnylyzedViewDataModel view, AnylyzedViewModelDataModel viewModel)
        {
            if (view != null && viewModel != null && viewModel.ImplementsBindableInterface)
            {
                var viewModelContent = _viewModelFilesContent[viewModel.Name];

                foreach (var property in view.BindingProperties)
                {
                    foreach (var line in viewModelContent)
                    {
                        if (line.Contains(property))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private List<string> PrepareViewFileContent (List<string> fileContent)
        {
            List<string> updatedFileContent = _fileEditorService.RemoveEmptyLines(fileContent);
            updatedFileContent = _fileEditorService.RemoveCommentedLines(updatedFileContent, "<!--", "-->");

            return updatedFileContent;
        }

        private string NameWithoutType (string fileName)
        {
            string formattedName = fileName;

            if (formattedName.EndsWith(".xaml") || formattedName.EndsWith(".cs"))
            {
                formattedName = formattedName.Remove(formattedName.LastIndexOf("."));
            }

            return formattedName;
        }

        private string NameWithoutSuffix(string fileName, bool isViewSuffix)
        {
            var simpleName = NameWithoutType(fileName);

            int substringLength = isViewSuffix ? 4 : 9;

            string fileNameWithoutSuffix = simpleName.Substring(0, simpleName.Length - substringLength);

            return fileNameWithoutSuffix;
        }

        private AnylyzedViewDataModel GetViewByName(string viewName, List<AnylyzedViewDataModel> views)
        {
            foreach (var view in views)
            {
                if (NameWithoutType(view.Name) == NameWithoutType(viewName))
                {
                    return view;
                }
            }

            return null;
        }

        private AnylyzedViewModelDataModel GetViewModelByName(
            string viewModelName,
            List<AnylyzedViewModelDataModel> viewModels)
        {
            foreach (var viewModel in viewModels)
            {
                if (NameWithoutType(viewModel.Name) == NameWithoutType(viewModelName))
                {
                    return viewModel;
                }
            }

            return null;
        }

        private AnylyzedViewModelDataModel GetViewModelByViewName(
            string viewName,
            List<AnylyzedViewModelDataModel> viewModels)
        {
            foreach (var viewModel in viewModels)
            {
                string viewSimpleName = NameWithoutSuffix(viewName, true);
                string viewModelSimpleName = NameWithoutSuffix(viewModel.Name, false);

                if (viewSimpleName == viewModelSimpleName)
                {
                    return viewModel;
                }
            }

            return null;
        }
    }
}
