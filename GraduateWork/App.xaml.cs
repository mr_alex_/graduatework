﻿using System;
using GraduateWork.Services;
using GraduateWork.ViewModels;
using GraduateWork.Views;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GraduateWork
{
    public partial class App : PrismApplication
    {
        public App()
        {
            InitializeComponent();
        }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(AnalyzerSettingsView)}");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // services
            containerRegistry.RegisterInstance<IAppFilesService>(Container.Resolve<AppFilesService>());
            containerRegistry.RegisterInstance<IFileEditorService>(Container.Resolve<FileEditorService>());
            containerRegistry.RegisterInstance<IMvvmAnalyzer>(Container.Resolve<MvvmAnalyzer>());

            // navigation
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<AnalyzerSettingsView, AnalyzerSettingsViewModel> ();
            containerRegistry.RegisterForNavigation<FilesListView, FilesListViewModel>();
            containerRegistry.RegisterForNavigation<PatternsAnalyzeResultView, PatternsAnalyzeResultViewModel>();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
