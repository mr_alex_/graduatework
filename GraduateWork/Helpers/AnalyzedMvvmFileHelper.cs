﻿using System;
using System.Collections.Generic;
using GraduateWork.Models;

namespace GraduateWork.Helpers
{
    public static class AnalyzedMvvmFileHelper
    {
        public static int CalculateMvvmPropsCount(Func<AnalyzedMvvmFileModel, bool> expression, List<AnalyzedMvvmFileModel> mvvmResults)
        {
            int count = 0;

            foreach (var mvvmItem in mvvmResults)
            {
                if (expression(mvvmItem))
                {
                    count++;
                }
            }

            return count;
        }
    }
}
