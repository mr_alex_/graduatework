﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using GraduateWork.Enums;
using GraduateWork.Services;
using GraduateWork.Views;
using Prism.Navigation;
using Xamarin.Forms;

namespace GraduateWork.ViewModels
{
    public class AnalyzerSettingsViewModel : ViewModelBase
    {
        private IAppFilesService _appFilesService;

        private List<string> _appFiles;

        public AnalyzerSettingsViewModel(
            INavigationService navigationService,
            IAppFilesService appFilesService
        ) : base(navigationService)
        {
            _appFilesService = appFilesService;

            AnalyzeAppCommand = new Command(OnAnalyzeAppCommand);
            ShowFilesCommand = new Command(OnShowFilesCommand);

            _selecteAppBudget = AppConstants.AppBudget.Values.ToList().First();
            _selectedDevelopTime = AppConstants.TimeToDevelop.Values.ToList().First();

            _appFiles = new List<string>();
        }

        public ICommand AnalyzeAppCommand { get; }

        public ICommand ShowFilesCommand { get; }

        public ICommand ClearCommand { get; }

        private bool _buttonsEnabled;
        public bool ButtonsEnabled
        {
            get => _buttonsEnabled;
            set => SetProperty(ref _buttonsEnabled, value);
        }

        public List<string> AppSizeList
        {
            get => Enum.GetNames(typeof(AppSize)).ToList();
        }

        private AppSize _selectedAppSize;
        public AppSize SelectedAppSize
        {
            get => _selectedAppSize;
            set
            {
                SetProperty(ref _selectedAppSize, value);

                SetDevelopTimeBySize(value);
                SetAppBudgetBySize(value);
            }
        }

        public List<string> AppTypeList
        {
            get => AppConstants.AppTypes;
        }

        private string _selectedAppType;
        public string SelectedAppType
        {
            get => _selectedAppType;
            set
            {
                SetProperty(ref _selectedAppType, value);

                // As this app logic implemented only for Xamarin / .Net project type
                // we always set the Xamarin / .Net project type as selected
                SetProperty(ref _selectedAppType, AppConstants.AppTypes.First());
            }
        }

        public List<string> DevelopTimeList
        {
            get => AppConstants.TimeToDevelop.Values.ToList();
        }

        private string _selectedDevelopTime;
        public string SelectedDevelopTime
        {
            get => _selectedDevelopTime;
            set => SetProperty(ref _selectedDevelopTime, value);
        }

        public List<string> AppBudgetList
        {
            get
            {
                List<string> budgetList = new List<string>();

                foreach(var value in AppConstants.AppBudget.Values)
                {
                    budgetList.Add(value.ToString());
                }

                return budgetList;
            }
        }

        private int _selecteAppBudget;
        public int SelectedAppBudget
        {
            get => _selecteAppBudget;
            set => SetProperty(ref _selecteAppBudget, value);
        }

        private string _projectDirectoryPath;
        public string ProjectDirectoryPath
        {
            get => _projectDirectoryPath;
            set => SetProperty(ref _projectDirectoryPath, value);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName == nameof(ProjectDirectoryPath))
            {
                // ToDo: fix the case when the string contains only whitespaces
                ButtonsEnabled = ProjectDirectoryPath.Length > 0 && ProjectDirectoryPath != string.Empty;
            }
        }

        private void SetAppBudgetBySize(AppSize projectSize)
        {
            SelectedAppBudget = AppConstants.AppBudget[projectSize];
        }

        private void SetDevelopTimeBySize(AppSize projectSize)
        {
            SelectedDevelopTime = AppConstants.TimeToDevelop[projectSize];
        }

        private async void OnAnalyzeAppCommand()
        {
            _appFiles = _appFilesService.GetAllFilesPaths(_projectDirectoryPath).ToList();

            if (_appFiles.Count > 0)
            {
                var navigationParams = new NavigationParameters
                {
                    { NavigationConstants.AppFilesPaths, _appFiles }
                };

                await NavigationService.NavigateAsync($"{nameof(PatternsAnalyzeResultView)}", navigationParams);
            }
        }

        private async void OnShowFilesCommand()
        {
            if (_projectDirectoryPath.Length > 0)
            {
                var navigationParams = new NavigationParameters
                {
                    { NavigationConstants.AppDirectoryPath, _projectDirectoryPath }
                };

                await NavigationService.NavigateAsync($"{nameof(FilesListView)}", navigationParams);
            }
        }
    }
}
