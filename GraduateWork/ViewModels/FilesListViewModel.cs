﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using GraduateWork.Enums;
using GraduateWork.Services;
using Prism.Navigation;
using Xamarin.Forms;

namespace GraduateWork.ViewModels
{
    public class FilesListViewModel : ViewModelBase
    {
        private IAppFilesService _appFilesService;

        private List<string> _loadedAppFilesPaths;

        public FilesListViewModel(
            INavigationService navigationService,
            IAppFilesService appFilesService
        ) : base(navigationService)
        {
            _appFilesService = appFilesService;

            _loadedAppFilesPaths = new List<string>();
            _appFiles = new ObservableCollection<string>();
        }

        private ObservableCollection<string> _appFiles;
        public ObservableCollection<string> AppFiles
        {
            get { return _appFiles; }
            set => SetProperty(ref _appFiles, value);
        }

        private string _selectedAppFile;
        public string SelectedAppFile
        {
            get { return _selectedAppFile; }
            set => SetProperty(ref _selectedAppFile, value);
        }

        private string _fileContent;
        public string FileContent
        {
            get { return _fileContent; }
            set => SetProperty(ref _fileContent, value);
        }

        private int _fileLinesCount;
        public int FileLinesCount
        {
            get { return _fileLinesCount; }
            set => SetProperty(ref _fileLinesCount, value);
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);

            if (args.PropertyName == nameof(SelectedAppFile))
            {
                LoadFileContent(SelectedAppFile);
            }
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.AppDirectoryPath))
            {
                string path = parameters.GetValue<string>(NavigationConstants.AppDirectoryPath);
                LoadAppFiles(path);
            }
        }

        private void LoadAppFiles (string path)
        {
            _loadedAppFilesPaths =_appFilesService.GetAllFilesPaths(
                path, new List<FileType>() { FileType.cs, FileType.xaml }
            ).ToList();

            if (_loadedAppFilesPaths.Count > 0)
            {
                AppFiles = new ObservableCollection<string>();

                foreach (var fileNamePath in _loadedAppFilesPaths)
                {
                    string name = _appFilesService.GetShortFileNameFromPath(fileNamePath);
                    AppFiles.Add(name);
                }
            }
        }

        private void LoadFileContent(string fileName)
        {
            try
            {
                string path = string.Empty;

                foreach (var filePath in _loadedAppFilesPaths)
                {
                    if (filePath.EndsWith(fileName))
                    {
                        if (_appFilesService.GetShortFileNameFromPath(filePath) == fileName)
                        {
                            path = filePath;
                        }
                    }
                }

                FileContent = _appFilesService.LoadFileContent(path);

                FileLinesCount = _appFilesService.GetFileLinesCount(path);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
