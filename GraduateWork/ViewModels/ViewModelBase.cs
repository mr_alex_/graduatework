﻿using System.Windows.Input;
using Prism.Mvvm;
using Prism.Navigation;
using Xamarin.Forms;

public abstract class ViewModelBase : BindableBase, INavigationAware, IDestructible
{
    protected INavigationService NavigationService { get; }

    public ViewModelBase(INavigationService navigationService)
    {
        NavigationService = navigationService;

        BackCommand = new Command(OnBackCommand);
    }

    public ICommand BackCommand { get; }

    // INavigationAware
    public virtual void OnNavigatedFrom(INavigationParameters parameters)
    {
    }

    // INavigationAware
    public virtual void OnNavigatedTo(INavigationParameters parameters)
    {
    }

    // IDestructible
    public virtual void Destroy()
    {
    }

    protected virtual async void OnBackCommand()
    {
        await NavigationService.GoBackAsync();
    }
}