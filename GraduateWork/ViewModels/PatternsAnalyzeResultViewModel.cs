﻿using System;
using System.Collections.Generic;
using GraduateWork.Helpers;
using GraduateWork.Models;
using GraduateWork.Services;
using Prism.Navigation;

namespace GraduateWork.ViewModels
{
    public class PatternsAnalyzeResultViewModel : ViewModelBase
    {
        private IMvvmAnalyzer _mvvmAnalyzer;
        private IAppFilesService _appFilesService;

        public PatternsAnalyzeResultViewModel(
            INavigationService navigationService,
            IMvvmAnalyzer mvvmAnalyzer,
            IAppFilesService appFilesService)
            : base(navigationService)
        {
            _mvvmAnalyzer = mvvmAnalyzer;
            _appFilesService = appFilesService;
        }

        private List<AnalyzedMvvmFileModel> _mvvmResults;
        public List<AnalyzedMvvmFileModel> MvvmResults
        {
            get { return _mvvmResults; }
            set => SetProperty(ref _mvvmResults, value);
        }

        private int _totalViewsCount;
        public int TotalViewsCount
        {
            get { return _totalViewsCount; }
            set => SetProperty(ref _totalViewsCount, value);
        }

        private int _viewsWithRefsCount;
        public int ViewsWithRefsCount
        {
            get { return _viewsWithRefsCount; }
            set => SetProperty(ref _viewsWithRefsCount, value);
        }

        private int _viewsWithViewModelsCount;
        public int ViewsWithViewModelsCount
        {
            get { return _viewsWithViewModelsCount; }
            set => SetProperty(ref _viewsWithViewModelsCount, value);
        }

        private int _totalViewModelsCount;
        public int TotalViewModelsCount
        {
            get { return _totalViewModelsCount; }
            set => SetProperty(ref _totalViewModelsCount, value);
        }

        private int _vMsWithViewsCount;
        public int VMsWithViewsCount
        {
            get { return _vMsWithViewsCount; }
            set => SetProperty(ref _vMsWithViewsCount, value);
        }

        private int _vMsWithRefsCount;
        public int VMsWithRefsCount
        {
            get { return _vMsWithRefsCount; }
            set => SetProperty(ref _vMsWithRefsCount, value);
        }

        private int _bindableVMsCount;
        public int BindableVMsCount
        {
            get { return _bindableVMsCount; }
            set => SetProperty(ref _bindableVMsCount, value);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            if (parameters.ContainsKey(NavigationConstants.AppFilesPaths))
            {
                var paths = parameters.GetValue<List<string>>(NavigationConstants.AppFilesPaths);

                MvvmResults = new List<AnalyzedMvvmFileModel>(
                    _mvvmAnalyzer.AnalyzeMvvmImplementation(paths)
                );

                TotalViewsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewName.Contains(".xaml"), MvvmResults);
                ViewsWithRefsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewHasReferences, MvvmResults);
                ViewsWithViewModelsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewHasBindedProperties, MvvmResults);

                TotalViewModelsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelName.Contains(".cs"), MvvmResults);
                VMsWithViewsCount = ViewsWithViewModelsCount;
                VMsWithRefsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelHasReferences, MvvmResults);
                BindableVMsCount = AnalyzedMvvmFileHelper.CalculateMvvmPropsCount((item) => item.ViewModelIsBindable, MvvmResults);

                const string path = @"/Users/alexsir/Projects/TestApp/test-app/TestProject/";

                _appFilesService.SaveAnalyzedMvvmInfo(path + "MVVM_Analysing_Info.txt", MvvmResults);
                _appFilesService.SaveAnalyzedMvvmFilesContent(path + "Analysed_MVVM_Files.txt", MvvmResults);
            }
        }
    }
}
    